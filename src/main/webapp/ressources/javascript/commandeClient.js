$(document).ready(function() {
	$("#codeArticle_search").on("keypress", function(e) {

		if (e.which == '13'){
			var codeArticle = $("#codeArticle_search").val();
			if(verifierClient() && codeArticle) {
				searchArticle(codeArticle);
			}
		}
	});
	$("#listClients").on("change", function(){
		if(verifierClient()){
			$("#clientNotSelectedBlock").hide(), function() { $("#clientNotSelectedBlock").hide()};
			creerCommande($("#listClients option:selected").val());
		}
	});
	$("#btnEnregisterCommande").on("click", function() {
		$.getJSON("enregistrerCommande",function(data) {
			if(data){
				window.location=""+data;
			}
			
			})
	});
	
	$("#notFoundMsgBlock").hide();
	$("#clientNotSelectedBlock").hide();
	$("#unexceptedErrorMsg").hide();
	
	
});

function updateDetailCommande(idCommande) {
	var json =$.parseJSON($("#json" + idCommande).text());
	var detailHtml = "";
	if(json) {
		for(var index = 0; index < json.length; index++){
		detailHtml += 
					"<tr>"+
					"<td>" + json[index].article.codeArticle + "</td>"+
					"<td>" + json[index].quantite + "</td>"+
					"<td>" + json[index].quantite + "</td>"+
					"<td>0</td>"+
					"</tr>";
		}
		$("#detailCommande").html(detailHtml);
	}
}

function verifierClient() {
	var idClient = $("#listClients option:selected").val();
	if(idClient) {
		if(idClient === "-1") {
			$("#clientNotSelectedBlock").show(), function() { $("#clientNotSelectedBlock").show()};
			return false;
		}return true;
	}
}

function creerCommande(idClient) {
	if(idClient){	
	$.getJSON("creerCommande", {
			idClient: idClient,
			ajax: true
		},
		function(data) {
			console.log("client a ete mise a jour");
		})
}
}

function searchArticle(codeArticle){
	if(codeArticle){
		var detailHtml = "";
		$.getJSON("ajouterLigne", {
			codeArticle: codeArticle,
			ajax: true
		}, 
		function(data) {
			if(data){
				var total = data.quantite*data.prixUnitaire;
				if($("#qte" + data.article.idArticle).length > 0 && $("#total" + data.article.idArticle).length > 0){
					$("#qte" + data.article.idArticle).text(data.quantite);
					$("#total" + data.article.idArticle).text(total);
				}else{
					detailHtml += 
						"<tr id='ligne" + data.article.idArticle + "' >"+
						"<td>" + data.article.codeArticle  + "</td>"+
						"<td id='qte" + data.article.idArticle + "'>"  + data.quantite + " </td>"+
						"<td>" + data.prixUnitaire + "</td>"+
						"<td id='total" + data.article.idArticle + "' >" + total + "</td>"+
						"<td><button class='btn btn-link' onclick='supprimerLigneCommmande("+ data.article.idArticle + ")' ><i class='fa fa-trash-o'></i></button></td>"+
						"</tr>";
					$("#detailNouvelleCommande").append(detailHtml);
					
				}
				$("#notFoundMsgBlock").hide(), function() { $("#notFoundMsgBlock").hide()};
				$("#codeArticle_search").val("");
			}
		}).fail(function() {
			$("#notFoundMsgBlock").show(), function() { $("#notFoundMsgBlock").show()};
		});
	}
}
	function supprimerLigneCommmande(idArticle){
		if($("#ligne" + idArticle).length > 0 ){
			$.getJSON("suppprimerLigne", {
				idArticle: idArticle,
				ajax: true
			},
			function(data) {
				if(data){
					$("#ligne" + idArticle).hide("slow", function() {$("#ligne" + idArticle).remove()});
				}
			});
	}
	
}