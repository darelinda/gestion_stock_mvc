<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gestions des commandes clients</title>
    
	 <link href="<%=request.getContextPath() %>/ressources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	  <link href="<%=request.getContextPath() %>/ressources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/startbootstrap-sb-admin-2/3.3.7+1/css/sb-admin-2.css">
	 <link href="<%=request.getContextPath() %>/ressources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
	<!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/3.0.3/metisMenu.css"> -->
	<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<!-- 	 <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

   <script src="<%=request.getContextPath() %>/ressources/vendor/jquery/jquery.min.js"></script>

  <!-- Bootstrap Core JavaScript -->
    <script src="<%=request.getContextPath() %>/ressources/javascript/commandeClient.js"></script>
    <script src="<%=request.getContextPath() %>/ressources/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<%=request.getContextPath() %>/ressources/javascript/commandeClient.js"></script>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/3.0.3/metisMenu.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/startbootstrap-sb-admin-2/3.3.7+1/js/sb-admin-2.min.js"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            
            <!-- /.navbar-top-links -->
        <%@ include file="/WEB-INF/views/menu_top/topMenu.jsp" %>    
		<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp" %>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><fmt:message code="commmande.client.nouveau"/></h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
				<div class="alert alert-danger" id="notFoundMsgBlock">
		           <fmt:message code="commande.client.article.not.found"/>
		        </div>
		        <div class="alert alert-danger" id="clientNotSelectedBlock">
		           <fmt:message code="commande.client.select.client.msg.erreur"/>
		        </div>
		        
		         <div class="alert alert-danger" id="unexceptedErrorMsg">
		           <fmt:message code="commande.client.select.client.unexcepted.erreur"/>
		        </div>
                <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                             <fmt:message code="commande.client.detail"/>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	<form  action="/commandeclient/enregistrerCommande"   enctype="multipart/form-data" role = "form">
                        	<div class ="form-row">
	                        	<div class="col-md-4 mb-3">
	                                 <label><fmt:message code="common.code"/></label>
	                                 <input class="form-control" placeholder="Code"  id = "codeCommande"  value = "${codeCde }" disabled/>
	                             </div>
	                              <div class="col-md-4 mb-3">
	                                 <label><fmt:message code="common.date"/></label>
	                                 <input  class="form-control" placeholder="Date" id = "dateCommande" value = "${dateCde }" disabled/>
	                             </div>
	                              <div class="col-md-4 mb-3">
	                                 <label><fmt:message code="common.client"/></label>
	                                 <select class="form-control" id = "listClients" name="clientSelect">
	                                 <option value ="-1"><fmt:message code="commande.client.select.client"/></option>
	                                 	<c:forEach items = "${clients }" var = "clt">
	                                 		<option value ="${clt.getIdClient()}">${clt.getNom() }</option>
	                                 	</c:forEach>
	                                 </select>
                             	</div>
                        	</div>
							<br/><br /><br />
                             <div class="panel-footer">
                             	<button type="button" id="btnEnregisterCommande" class="btn btn-primay"><i class=" fa fa-save"></i>&nbsp;<fmt:message code="common.enregister"/></button>
                             	<a href="<c:url value="/commandeclient/"/>" class="btn btn-danger"><i class=" fa fa-arrow-left">&nbsp;</i><fmt:message code="common.annuler"/></a>
                        	</div>
                        </form>
                            

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
             <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                             <fmt:message code="commande.client.detail"/>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
	                        <div class="form-row">
		                        <div class="col-md-4 mb-3">
		                        	<label><fmt:message code="common.article"/></label>
		                        	<input class= "form-control" type = "text" id ="codeArticle_search"  placeholder="saisir code article"/>
		                        </div>
	                        </div>
							<br/><br /><br />
                               <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
	                               	 <thead>
	                                 <tr>
	                                 <th><fmt:message code="common.article"/></th>
	                                    <th><fmt:message code="common.qte"/></th>
	                                    <th> <fmt:message code="common.prixUnitTTC"/></th>
	                                    <th> <fmt:message code="common.total"/></th> 
	                                    <th> <fmt:message code="common.actions"/></th>  
	                                   </tr>
	                                </thead>
	                                <tbody id = "detailNouvelleCommande">  
	                                </tbody>
                            </table>
                            </div>
                            </div>
                            <div class="row"><div class="col-sm-6">
                            <div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">Showing 11 to 20 of 57 entries
                            </div></div><div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                            <ul class="pagination"><li class="paginate_button previous" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"><a href="#">Previous</a></li>
                            <li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">1</a></li><li class="paginate_button active" aria-controls="dataTables-example" tabindex="0"><a href="#">2
                            </a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">3</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">4
                            </li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">5</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">6</a></li><li class="paginate_button next" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_next"><a href="#">Next</a></li></ul></div></div></div></div>
                            <!-- /.table-responsive -->
                        </div>
            
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>

</body>

</html>
