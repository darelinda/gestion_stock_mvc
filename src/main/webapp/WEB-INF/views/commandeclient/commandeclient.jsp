<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gestions des commandes clients</title>
    
	 <link href="<%=request.getContextPath() %>/ressources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	  <link href="<%=request.getContextPath() %>/ressources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/startbootstrap-sb-admin-2/3.3.7+1/css/sb-admin-2.css">
	 <link href="<%=request.getContextPath() %>/ressources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
	<!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/3.0.3/metisMenu.css"> -->
	<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<!-- 	 <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

   <script src="<%=request.getContextPath() %>/ressources/vendor/jquery/jquery.min.js"></script>

  <!-- Bootstrap Core JavaScript -->
    <script src="<%=request.getContextPath() %>/ressources/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<%=request.getContextPath() %>/ressources/javascript/commandeClient.js"></script>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/3.0.3/metisMenu.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/startbootstrap-sb-admin-2/3.3.7+1/js/sb-admin-2.min.js"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            
            <!-- /.navbar-top-links -->
        <%@ include file="/WEB-INF/views/menu_top/topMenu.jsp" %>    
		<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp" %>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><fmt:message code="common.client.commande"/></h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                
                <div class="row">
                     <div class="col-lg-12">
                       <ol class="breadcrumb">
	 						<li><a href="<c:url value="/commandeclient/nouveau"/>">
	 								<i class=" fa fa-plus">&nbsp; <fmt:message code="common.ajouter"/></i>
	 							</a></li>
	 						<li><a href="#"><i class=" fa fa-download">&nbsp; <fmt:message code="common.exporter"/></i></a></li>
						 	<li><a href="#"><i class=" fa fa-upload">&nbsp; <fmt:message code="common.importer"/></i></li>
						</ol>
					</div>
                </div>
                
                                
                <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                             <fmt:message code="commande.client.liste"/>
                        </div>
                        <!-- /.panel-heading  href = "javascript:void(0);" -->
                        <div class="panel-body">
                               <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                               	 <thead>
                                 <tr>
                                 <th><fmt:message code="common.code"/></th>
                                    <th><fmt:message code="common.date"/></th>
                                    <th> <fmt:message code="common.client"/></th>
                                    <th> <fmt:message code="common.total"/></th>   
                                     <th> <fmt:message code="common.actions"/></th>
                                  </tr>
                                </thead>
                                
                                <tbody>  
    								<c:forEach items= "${commandesClient}" var = "cdeClient">
    								<tr>
    									 <td>${cdeClient.getCode() }</td>
    									 <td>${cdeClient.getDateCommande() }</td>
    									 <td>${cdeClient.getClient().getNom() }</td>
    									 <td>${cdeClient.getTotalCommande() }</td>
    									 <td >
                                        	<textArea id ="json${ cdeClient.getIdCommandeClient()}" style="display: none;">${ cdeClient.getLigneCommandeJson()}</textArea> 
	                                        <button class="btn btn-link" onclick="updateDetailCommande(${ cdeClient.getIdCommandeClient()});" title="Detail commande"><i class=" fa fa-th-list"></i></button>&nbsp; &nbsp;|
	                                        <c:url value ="/commandeclient/modifier/${ cdeClient.getIdCommandeClient()}"  var ="urlModifier" />
	                                        <a href="${ urlModifier}"><i class=" fa fa-edit" title="modifier commande"></i></a>&nbsp; &nbsp;|
	                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#modalCommandeClient${cdeClient.getIdCommandeClient()} "><i class=" fa fa-trash-o"></i></a>
	                                         <div class="modal fade" id="modalCommandeClient${cdeClient.getIdCommandeClient() }" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		                               		<div class="modal-dialog">
		                                    		<div class="modal-content">
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                            <h4 class="modal-title" id="myModalLabel"><fmt:message code="common.confirm.suppression"/></h4>
			                                        </div>
			                                        <div class="modal-body">
			                                        	<fmt:message code="client.confirm.suppression.msg"/>  
			                                        </div>
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message code="common.annuler"/></button>
			                                            <c:url value ="/commandeclient/supprimer/${cdeClient.getIdCommandeClient() }" var="urlSuppression"/>
			                                            <a href="${urlSuppression}" class="btn btn-danger" title="Supprimer commande"> <i class=" fa fa-trash-o"></i> &nbsp; <fmt:message code="common.confirmer"/></a>
			                                        </div>
			                                    </div>
		                                    <!-- /.modal-content -->
		                                </div>
                                <!-- /.modal-dialog -->
                        			 	</div>
                                    </td>	 
    							<tr>		 
    								</c:forEach>
                                </tbody>
                            </table>
                            </div>
                            </div>
                            <div class="row"><div class="col-sm-6">
                            <div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">Showing 11 to 20 of 57 entries
                            </div></div><div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                            <ul class="pagination"><li class="paginate_button previous" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"><a href="#">Previous</a></li>
                            <li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">1</a></li><li class="paginate_button active" aria-controls="dataTables-example" tabindex="0"><a href="#">2
                            </a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">3</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">4
                            </li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">5</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">6</a></li><li class="paginate_button next" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_next"><a href="#">Next</a></li></ul></div></div></div></div>
                            <!-- /.table-responsive -->
                            

                        </div>
                        
                <!-- detail de la commande -->         
               <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                             <fmt:message code="commande.client.detail"/>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                               <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                               	 <thead>
                                 <tr>
                                 <th><fmt:message code="common.article"/></th>
                                    <th><fmt:message code="common.qte"/></th>
                                    <th> <fmt:message code="common.prixUnitTTC"/></th>
                                    <th> <fmt:message code="common.total"/></th>   
                                   </tr>
                                </thead>
                                
                                <tbody id = "detailCommande">  
    								
                                </tbody>
                            </table>
                            </div>
                            </div>
                            <div class="row"><div class="col-sm-6">
                            <div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">Showing 11 to 20 of 57 entries
                            </div></div><div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                            <ul class="pagination"><li class="paginate_button previous" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"><a href="#">Previous</a></li>
                            <li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">1</a></li><li class="paginate_button active" aria-controls="dataTables-example" tabindex="0"><a href="#">2
                            </a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">3</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">4
                            </li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">5</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">6</a></li><li class="paginate_button next" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_next"><a href="#">Next</a></li></ul></div></div></div></div>
                            <!-- /.table-responsive -->
                            

                        </div>
                        
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
 

</body>

</html>
