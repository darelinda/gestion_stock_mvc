<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ajouter Client</title>
         <link href="<%=request.getContextPath() %>/ressources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    
	<!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/startbootstrap-sb-admin-2/3.3.7+1/css/sb-admin-2.css">
	 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/3.0.3/metisMenu.css">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	 <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/3.0.3/metisMenu.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/startbootstrap-sb-admin-2/3.3.7+1/js/sb-admin-2.min.js"></script>

</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
       
            <!-- /.navbar-top-links -->
        <%@ include file="/WEB-INF/views/menu_top/topMenu.jsp" %>    
		<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp" %>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><fmt:message code="client.nouveau"/></h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
             <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                             <fmt:message code="client.nouveau"/>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	<c:url value ="/client/enregister" var ="urlEnregister" />
                        	<f:form modelAttribute="client" action="${urlEnregister }" method ="post"  enctype="multipart/form-data" role = "form">
                        		<f:hidden path="idClient"/>
                             <div class="form-group">
                                 <label><fmt:message code="common.nom"/></label>
                                 <f:input path="nom" class="form-control" placeholder="Nom"/>
                             </div>
                              <div class="form-group">
                                 <label><fmt:message code="common.prenom"/></label>
                                 <f:input path="prenom" class="form-control" placeholder="Prenom"/>
                             </div>
                              <div class="form-group">
                                 <label><fmt:message code="common.adresse"/></label>
                                 <f:input path="adresse" class="form-control" placeholder="Adresse"/>
                             </div>
                              <div class="form-group">
                                 <label><fmt:message code="common.mail"/></label>
                                 <f:input path="email" class="form-control" placeholder="Email"/>
                             </div>
                             <div class="form-group">
                                  <label><fmt:message code="common.photo"/></label>
                                  <input type="file" name="file">
                              </div>
                             <div class="panel-footer">
                             	<button type="submit" class="btn btn-primay"><i class=" fa fa-save"></i>&nbsp;<fmt:message code="common.enregister"/></button>
                             	<a href="<c:url value="/client/"/>" class="btn btn-danger"><i class=" fa fa-arrow-left">&nbsp;</i><fmt:message code="common.annuler"/></a>
                        	</div>
                        </f:form>
                            

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>

</body>

</html>
