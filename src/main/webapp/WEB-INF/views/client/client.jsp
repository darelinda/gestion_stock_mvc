<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gestion des clients</title>
    
    <!-- CSS -->
     <link href="<%=request.getContextPath() %>/ressources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/startbootstrap-sb-admin-2/3.3.7+1/css/sb-admin-2.css">
	 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/3.0.3/metisMenu.css">
	 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.css">
	 <link rel="https://cdn.datatables.net/responsive/2.0.0/css/responsive.dataTables.css">
	 
	 <!-- JS -->
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	 <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/3.0.3/metisMenu.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/startbootstrap-sb-admin-2/3.3.7+1/js/sb-admin-2.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.js"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            
            <!-- /.navbar-top-links -->
        <%@ include file="/WEB-INF/views/menu_top/topMenu.jsp" %>    
		<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp" %>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><fmt:message code="common.client"/></h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                
                 <div class="row">
                      	<div class="col-lg-12">
	                        <ol class="breadcrumb">
		  						<li><a href="<c:url value="/client/nouveau"/>">
		  							<i class=" fa fa-plus">&nbsp; <fmt:message code="common.ajouter"/></i></a></li>
		  							<c:url value ="/client/export/"  var ="urlExport" />
		  						<li><a href="${urlExport }"><i class=" fa fa-download">&nbsp; <fmt:message code="common.exporter"/></i></a></li>
		 					 	<li><a href="#"><i class=" fa fa-upload">&nbsp; <fmt:message code="common.importer"/></i></li>
							</ol>
						</div>
                      </div>
                
                <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                             <fmt:message code="client.liste"/>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label>Show <select name="dataTables-example_length" aria-controls="dataTables-example" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="dataTables-example"></label></div></div></div><div class="row"><div class="col-sm-12"><table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
                                <thead>
                                 <tr role="row">
                                 <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" style="width: 86px;" aria-sort="ascending">
                                    <fmt:message code="common.photo"/></th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 108px;">
                                    <fmt:message code="common.nom"/></th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 98px;">
                                    <fmt:message code="common.prenom"/></th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 73px;">
                                    <fmt:message code="common.adresse"/></th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 51px;">
                                   <fmt:message code="common.mail"/></th>
                                   
                                     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 51px;">
                                   <fmt:message code="common.actions"/></th>
                                   </tr>
                                </thead>
                                
                                <tbody>  
                                
                                <c:forEach items ="${clients }" var = "client">
                                	<tr class="gradeA odd" role="row">
                                        <td class="sorting_1"><img src ="${client.getPhoto() }" width="100px" height ="70px"/></td>
                                        <td>${client.getNom() }</td>
                                        <td>${client.getPrenom() }</td>
                                        <td class="center">${client.getAdresse() }</td>
                                        <td class="center">${client.getEmail() }</td>
                                        <td class="center">
                                        	<c:url value ="/client/modifier/${ client.getIdClient()}"  var ="urlModifier" />
	                                        <a href="${ urlModifier}"><i class=" fa fa-edit"></i></a>&nbsp; &nbsp;|
	                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#modalClient${client.getIdClient()} "><i class=" fa fa-trash-o"></i></a>
	                                         <div class="modal fade" id="modalClient${client.getIdClient() }" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		                               		<div class="modal-dialog">
		                                    		<div class="modal-content">
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                            <h4 class="modal-title" id="myModalLabel"><fmt:message code="common.confirm.suppression"/></h4>
			                                        </div>
			                                        <div class="modal-body">
			                                        	<fmt:message code="client.confirm.suppression.msg"/>  
			                                        </div>
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message code="common.annuler"/></button>
			                                            <c:url value ="/client/supprimer/${client.getIdClient() }" var="urlSuppression"/>
			                                            <a href="${urlSuppression}" class="btn btn-danger"><i class=" fa fa-trash-o"></i> &nbsp; <fmt:message code="common.confirmer"/></a>
			                                        </div>
			                                    </div>
		                                    <!-- /.modal-content -->
		                                </div>
                                <!-- /.modal-dialog -->
                        			 	</div>
                                    </td>
                                   </tr>
                                </c:forEach>     
                                </tbody>
                            </table>
                            </div>
                            </div>
                            <div class="row"><div class="col-sm-6">
                            <div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">Showing 11 to 20 of 57 entries
                            </div></div><div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                            <ul class="pagination"><li class="paginate_button previous" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"><a href="#">Previous</a></li>
                            <li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">1</a></li><li class="paginate_button active" aria-controls="dataTables-example" tabindex="0"><a href="#">2
                            </a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">3</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">4
                            </li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">5</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">6</a></li><li class="paginate_button next" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_next"><a href="#">Next</a></li></ul></div></div></div></div>
                            <!-- /.table-responsive -->
                            

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
 

</body>

</html>
