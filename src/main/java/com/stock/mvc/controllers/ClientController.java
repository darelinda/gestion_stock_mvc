package com.stock.mvc.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.stock.mvc.entities.Client;
import com.stock.mvc.export.FileExporter;
import com.stock.mvc.services.IClientService;
import com.stock.mvc.services.IFlickrService;

@Controller
@RequestMapping(value = "/client")
public class ClientController {
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private IFlickrService flickrService;
	
	@Autowired
	@Qualifier("clientExporter")
	private FileExporter exporter;

	@RequestMapping(value ="/")	
	public String client(Model model) {
		//model est une MAP cl�/valeur
		List<Client> clients = clientService.selectAll();
		if(clients == null) {
			clients = new ArrayList<Client>();
		}
		model.addAttribute("clients" , clients);
			return "client/client";
		}
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterClient(Model model){
		Client client = new Client();
		model.addAttribute("client", client);		
		return "client/ajouterClient";
	}
	
	@RequestMapping(value = "/enregister", method = RequestMethod.POST)
	public String enregistrerClient(Model model, Client client, MultipartFile file){
		String photoUrl = null;
		if(client!=null){
			if(file!=null && !file.isEmpty()){
				InputStream stream =null;
				try{
					stream = file.getInputStream();
					photoUrl = flickrService.savePhoto(stream, client.getNom());
					client.setPhoto(photoUrl);
				}catch (Exception e){
					e.printStackTrace();
				}finally{
					try{
						stream.close();
					}catch (IOException e){
						e.printStackTrace();
					}
				}	
				}
			}
		if (client.getIdClient() != null){
			clientService.update(client);
		}
		else{
			//client.setPhoto("https://c1.staticflickr.com/5/4906/31735208177_6de4f8ded0_q.jpg");
			clientService.save(client);
		}
		return "redirect:/client/";
	}
	
	@RequestMapping(value = "/modifier/{idClient}")
	public String modifierClient (Model model, @PathVariable Long idClient){
		
		if(idClient != null){
			Client client = clientService.getById(idClient);
			if (client != null){
				model.addAttribute("client", client);
			}
		}
		return "client/ajouterClient";
	}
	
	@RequestMapping(value = "/supprimer/{idClient}")
	public String supprimerClient (Model model, @PathVariable Long idClient){
		if(idClient != null){
			Client client = clientService.getById(idClient);
			if (client != null){
				clientService.remove(idClient);
			}
		}
		
		return "redirect:/client/";
		
	}
	
	@RequestMapping(value ="/export/")	
	public String exportClient(HttpServletResponse response) {
		
		exporter.exportDataToExcel(response, null, null);
		return "client/client";
	}
	
}
