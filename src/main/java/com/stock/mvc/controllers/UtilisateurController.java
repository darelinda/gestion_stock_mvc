package com.stock.mvc.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.stock.mvc.entities.Utilisateur;
import com.stock.mvc.services.IUtilisateurService;

@Controller
@RequestMapping(value = "/utilisateur")
public class UtilisateurController {
	
	@Autowired
	private IUtilisateurService utilisateurService;
	
	@RequestMapping(value = "/")
	public String listUtilisateur (Model model){
		
		List<Utilisateur> listUtlisateurs = utilisateurService.selectAll();
		if(listUtlisateurs == null) {
			listUtlisateurs = new ArrayList<Utilisateur>();
		}
		model.addAttribute("listUtlisateurs" , listUtlisateurs);
			
		return"utilisateur/utilisateur";
	}

}
