package com.stock.mvc.export;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.axis.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stock.mvc.entities.Client;
import com.stock.mvc.services.IClientService;
import com.stock.mvc.utils.ApplicationConstants;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFeatures;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

@Component("clientExporter")
public class ClientExport implements FileExporter {
	
	private static final String File_NAME = "Liste des clients";
	
	@Autowired
	private IClientService clientService;

	@Override
	public boolean exportDataToExcel(HttpServletResponse response, String fileName, String encodage) {
		
		if(StringUtils.isEmpty(fileName)){
			fileName = File_NAME;
		}
		
		if(StringUtils.isEmpty(encodage)){
			encodage = ApplicationConstants.DEFAULT_ENCODAGE;
		}
		response.setContentType(ApplicationConstants.EXCEL_CONTEXT_TYPE);
		response.setHeader(ApplicationConstants.CONTEXT_DISPOSITION, "attachement; filename" + fileName + ".xls");
		 WorkbookSettings workbookSettings = new WorkbookSettings();
		 workbookSettings.setEncoding(encodage);
		 try{
			 //creer un fichier excel
			 WritableWorkbook workBook = Workbook.createWorkbook(response.getOutputStream(), workbookSettings);
			 //creer une feuille excel
			 WritableSheet sheet  = workBook.createSheet(fileName, 0);
			// l'ent�te du tableau excel
			 /**
			  * Sheet Header
			  */
			Label labelNom = new Label(0, 0, ApplicationConstants.NOM_CLIENT);
			labelNom.setCellFeatures(new WritableCellFeatures());
			labelNom.getCellFeatures().setComment("NomClient");
			sheet.addCell(labelNom);
			
			Label labelPrenom = new Label(1, 0, ApplicationConstants.PRENOM_CLIENT);
			labelPrenom.setCellFeatures(new WritableCellFeatures());
			labelPrenom.getCellFeatures().setComment("PrenomClient");
			sheet.addCell(labelPrenom);
			
			Label labelAdresse = new Label(2, 0, ApplicationConstants.ADRESSE_CLIENT);
			labelAdresse.setCellFeatures(new WritableCellFeatures());
			labelAdresse.getCellFeatures().setComment("AdresseClient");
			sheet.addCell(labelAdresse);
			
			Label labelEmail = new Label(3, 0, ApplicationConstants.EMAIL_CLIENT);
			labelEmail.setCellFeatures(new WritableCellFeatures());
			labelEmail.getCellFeatures().setComment("AdresseClient");
			sheet.addCell(labelEmail);
			
			int currentRow = 1;
			List<Client> clients = clientService.selectAll();
			
			if (clients != null && !clients.isEmpty()){
				
				/**
				 * Writing in the sheet
				 */
				for( Client client : clients){
					sheet.addCell(new Label(0, currentRow, client.getNom()));
					sheet.addCell(new Label(1, currentRow, client.getPrenom()));
					sheet.addCell(new Label(2, currentRow, client.getAdresse()));
					sheet.addCell(new Label(3, currentRow, client.getEmail()));
					currentRow++;
				}
				CellView cellView = new CellView();
				cellView.setSize(500);
				sheet.setColumnView(0, cellView);
				sheet.setColumnView(1, cellView);
				sheet.setColumnView(2, cellView);
				sheet.setColumnView(3, cellView);
				
				/**
				 * Write to excel sheet
				 */
				workBook.write();
				
				/**
				 * Closing the workBook
				 */
				workBook.close();
				response.getOutputStream().flush();
				response.getOutputStream().close();
				
			}
			 return true;
		 }catch (Exception e){
			 e.printStackTrace();
			 return false;
		 }

	}

	@Override
	public boolean importDataFromExcel() {
		// TODO Auto-generated method stub
		return false;
	}

}
