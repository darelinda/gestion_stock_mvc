package com.stock.mvc.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IGenericDao;

@SuppressWarnings("unchecked")
public class GenericDaoImpl<E> implements IGenericDao<E> {
	
	@PersistenceContext
	protected EntityManager em;
	
	private Class<E> type;
	
	public GenericDaoImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class<E>) pt.getActualTypeArguments()[0];
	}
	
	public Class<E> getType() {
		return type;
	}

	@Override
	@Transactional
	public E save(E entity) {
		em.persist(entity);
		return entity;
	}

	@Override
	@Transactional
	public E update(E entity) {
		return em.merge(entity);
	}

	@Override
	@Transactional
	public List<E> selectAll() {
		Query query = em.createQuery("select t from " + type.getSimpleName() + " t");
		return query.getResultList();
	}

	@Override
	@Transactional
	public List<E> selectAll(String sortField, String sort) {
		Query query = em.createQuery("select t from " + type.getSimpleName() + "t order by" + sortField + " "+ sort);
		return query.getResultList();
	}

	@Override
	@Transactional
	public E getById(Long id) {
		return em.find(type, id);
	}

	@Override
	@Transactional
	public void remove(Long id) {
		E tab  = em.getReference(type, id);
		em.remove(tab);
		
	}

	@Override
	@Transactional
	public E findOne(String paraName, Object paraValue) {
		Query query = em.createQuery("select t from " + type.getSimpleName() + " t where " +paraName + " = :x ");
		query.setParameter("x", paraValue);
		return query.getResultList().size() > 0 ? (E) query.getResultList().get(0) : null;
		
	}

	@Override
	@Transactional
	public E findOne(String[] paraNames, Object[] paramValues) {
		if (paraNames.length != paramValues.length) {
			return null;
		}
		String queryString = "select e from " +type.getSimpleName() + "e where ";
		int len = paramValues.length;
		for(int i = 0; i < len; i++) {
			queryString +=" e." + paraNames[i] + "= :x" + i;
			if((i +1) < len) {
				queryString += "and ";
			}
		}
		Query query = em.createQuery(queryString);
		for(int i = 0; i<paramValues.length; i++) {
			query.setParameter("x" + i, paramValues[i]);
		}
		return query.getResultList().size() > 0 ? (E) query.getResultList().get(0) : null;
	}

	@Override
	@Transactional
	public int findCountBy(String paraName, String paraValue) {
		Query query = em.createQuery("select t from " + type.getSimpleName() + "t where " +paraName + " = :x ");
		query.setParameter(paraName, paraValue);
		return query.getResultList().size() > 0 ? ((Long) query.getSingleResult()).intValue() : 0;
	}

}
