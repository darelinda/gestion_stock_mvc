package com.stock.mvc.utils;

public class ApplicationConstants {
	
	private ApplicationConstants(){}
	
	public static final String EXCEL_CONTEXT_TYPE = "application/vnd.ms-excel";
	
	public static final String CONTEXT_DISPOSITION = "Context-disposition";
	
	public static final String DEFAULT_ENCODAGE = "ISO-8859-A";

	public static final String NOM_CLIENT = "NomClient";
	
	public static final String PRENOM_CLIENT = "PrenomClient";
	
	public static final String ADRESSE_CLIENT = "AdresseClient";
	
	public static final String EMAIL_CLIENT = "EmailClient";

}
